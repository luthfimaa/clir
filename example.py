from reader import PdfReader
from translator import DocumentTranslator
from indexer import Indexer
from retriever import Retriever


reader = PdfReader()
documents = reader.reads('pdfs_path')
translator = DocumentTranslator()
translated_documents = translator.translates(documents)

indexer = Indexer()
index = indexer.build_index(documents)
retriever = Retriever(index, indexer)
indexes = retriever.query('perang korupsi di nigeria')  # query in Indonesian but the most relevant document in english

print('indexes', indexes)
print()

for i in indexes[:3]:
    sentences = documents[i][:1000]
    print(sentences)
    print()
