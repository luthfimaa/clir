import pickle

def save_index(index):
    with open('index.pkl', 'wb') as f:
        pickle.dump(index, f)
        
def save_word_index(word_idx):
    with open('word_index.pkl', 'wb') as f:
        pickle.dump(word_idx, f)
