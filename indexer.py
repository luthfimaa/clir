import os
import re
import numpy as np
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class IndexPreprocessor:
    def __init__(self):
        factory = StemmerFactory()
        self.stemmer = factory.create_stemmer()
        
    def preprocess(self, doc):
        doc = self.clean(doc)
        tokens = self.tokenize(doc)
        tokens = [self.case_fold(token) for token in tokens]
        tokens = [self.stem(token) for token in tokens]
        return tokens
    
    def preprocess_all(self, docs):
#         preprocessed_docs = [self.preprocess(doc) for doc in docs]
        preprocessed_docs = []
        counter = 0
        for doc in docs:
            counter += 1
            print('preprocessing doc {} of {}'.format(counter, len(docs)))
            preprocessed_docs.append(self.preprocess(doc))
        
        return preprocessed_docs
    
    def clean(self, doc):
        return re.sub('[\n\t]', ' ', doc)
    
    def tokenize(self, doc):
        return re.findall('[A-Za-z]+', doc)
    
    def case_fold(self, token):
        return token.lower()
    
    def stem(self, token):
        return self.stemmer.stem(token)
    
class Indexer:
    def __init__(self):
        self.preprocessor = IndexPreprocessor()
        self.word_idx = {}
        
    def build_index(self, docs):
        doc_tokens = self.preprocessor.preprocess_all(docs)
        self.build_word_idx(doc_tokens)
        
        # build tf matrix
        print('building tf matrix')
        tf_matrix = np.zeros((len(self.word_idx), len(doc_tokens)))
        for i, doc in enumerate(doc_tokens):
            for token in doc:
                token_idx = self.word_idx[token]
                tf_matrix[token_idx, i] += 1 # count term occurence
        
        # build idf column
        idf = np.zeros(len(self.word_idx))
        for i in range(len(self.word_idx)):
            term_row = tf_matrix[i, :]
            df = len(np.nonzero(term_row))
            idf[i] = np.log(len(docs)/df)
        
        # build tf idf matrix
        print('building tf idf matrix')
        tf_idf_matrix = np.zeros(tf_matrix.shape)
        for word_idx in range(len(self.word_idx)):
            for doc_idx in range(len(docs)):
                tf_idf_matrix[word_idx, doc_idx] = tf_matrix[word_idx, doc_idx] * idf[word_idx]

        # normalize
        for doc_idx in range(len(docs)):
            doc_tf_sum = tf_idf_matrix[:, doc_idx].sum()
            tf_idf_matrix[:, doc_idx] /= doc_tf_sum
        
        return tf_idf_matrix

    def build_word_idx(self, doc_tokens):
        # flatten tokens from 2d to 1d list
        token_list = []
        for tokens in doc_tokens:
            for token in tokens:
                token_list.append(token)
                
        # build index that map token to int
        unique_token = enumerate(set(token_list))
        for i, t in unique_token:
            self.word_idx[t] = i
            
    def text2vector(self, sentence):
        vector = np.zeros(len(self.word_idx))
        for word in self.preprocessor.preprocess(sentence):
            if word in self.word_idx:
                idx = self.word_idx[word]
                vector[idx] += 1
        return np.array(vector)
