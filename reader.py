import textract
import os

class ConversionPreprocessor:
    def preprocess(self, base_string):
        string = str(base_string)
        string = ' '.join(string.split('\\n'))
        return string

class PdfReader:
    def __init__(self):
        self.preprocessor = ConversionPreprocessor()
        
    def read(self, path, fname):
        base_string = textract.process(os.path.join(path, fname))
        string = self.preprocessor.preprocess(base_string)
        return string
    
    def reads(self, path):
        files = os.listdir(path)
        pdf_files = [f for f in files if self._is_pdf(f)]
        strings = [self.read(path, f) for f in pdf_files]
        return strings
        
    def _is_pdf(self, file):
        return file.endswith('.pdf')
