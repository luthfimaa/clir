from googletrans import Translator

class DocumentTranslator:
    def __init__(self):
        self.api = Translator()
        
    def translate(self, doc, lang='id', chunk_size=50):
        # translate document per chunk to fix text too long error
        sentences = doc.split('.')
        translated = []
        for i in range(0, len(sentences), chunk_size):
            chunk = sentences[(i*chunk_size):((i+1)*chunk_size)]
            translated_chunk = self.api.translate(chunk, lang)
            translated += translated_chunk
        translated_texts = [t.text for t in translated]
        translated_doc = '.'.join(translated_texts)
        return translated_doc
    
    def translates(self, docs):
        translated_docs = []
        for i in range(len(docs)):
            print('[INFO] Translating document {} of {}'.format((i+1), (len(docs))))
            doc = docs[i]
            translated_docs.append(self.translate(doc))
        print('[INFO] Translation completed')
        return translated_docs
