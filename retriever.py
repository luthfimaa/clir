import numpy

class Retriever:
    def __init__(self, index, indexer):
        self.index = index
        self.indexer = indexer
        
    def query(self, query_text, n=10):
        query_vector = self.indexer.text2vector(query_text)
        query_vector /= query_vector.sum() # normalize
        indexes = self.sort_closest(query_vector)
        return indexes[:n]
    
    def sort_closest(self, query_vector):
        doc_n = self.index.shape[1]
        similarities = np.zeros(doc_n)
        for doc_idx in range(doc_n):
            col = self.index[:, doc_idx]
            similarities[doc_idx] = self.get_similarity(col, query_vector)
        indexes = list(reversed(np.argsort(similarities)))
        return indexes
    
    def get_similarity(self, col, query_vector):
        return (col * query_vector).sum()
